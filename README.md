## Suggested Interview Questions

### Getting to Know Them 

- Development Background 
- 
- What is your biggest strength when developing?  Weakness?
- 
- If you had an unlimited budget and timeline what project would you create? 
- 
- What is your favorite language, and why?

## Technical Questions

### GA SPECIFIC 

- I see you went to GA.  How do you think that has prepared you for a job in web development?

- Tell me about a time you solved a difficult problem in a group setting.

- Explain what React lifecycle methods are.
- 
- Explain the difference between state and props.
- 
- Why might you use a CSS framework like Bootstrap?  When would that be less-than-ideal?
- 
- How would you build a RESTful API?
- 
- Tell me about a project you built on your own.
- 
- What are some of the major differences between JS and Ruby?
- 

### React Specific Questions 

- Explain the React lifecycle methods. 

- What is a HOC

A higher-order component (HOC) is an advanced technique in React for reusing component logic. HOCs are not part of the React API. They are a pattern that emerges from React’s compositional nature.
A higher-order component is a function that takes a component and returns a new component.
HOC’s allow you to reuse code, logic and bootstrap abstraction. HOCs are common in third-party React libraries. The most common is probably Redux’s connect function. Beyond simply sharing utility libraries and simple composition, HOCs are the best way to share behavior between React Components. If you find yourself writing a lot of code in different places that does the same thing, you may be able to refactor that code into a reusable HOC.

- What is prop drilling?
- 
- Explain what an action, reducer and what the store is
- 
- Explain the tools you'd use to automate testing in React. 

- Describe when you'd use a ref in React. 

Refs are used for managing focus, selecting text and triggering animations. It also integrates with third-party libraries. Refs help in getting the reference to a DOM (Document Object Model) node. They return the node that we are referencing. Whenever we need DOM measurements, we can use refs.

- What are the benefits of Hooks? 

- Explain single source of truth 

- Explain the difference in state and props.

- Tell me about the difference between a functional and class component. 
